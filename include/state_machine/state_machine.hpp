#include <iostream>
#include <string>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <vector>
#include <unistd.h>
#include <ros/ros.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/convert.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/Altitude.h>
#include <mavros_msgs/PositionTarget.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <state_machine/DroneMsg.h>

enum DRONE_STATES{idle, initialize, mission_temp, mission_takeoff, mission_map, mission_land};
enum MISSION_STATUS{in_progress, done, failed};
enum DRONE_MODES{autotakeoff, autoland, autoloiter, velocity, position};

class StateMachine
{
    private:

    ros::NodeHandle nh_;

    public:

    StateMachine(ros::NodeHandle & nh);

    ros::Publisher state_machine_pub;
    ros::Subscriber droneMsg_sub;
    ros::Subscriber roverMsg_sub;

    void stateMachine();

    void droneMsgCallback(const state_machine::DroneMsg::ConstPtr& msg);
    void roverMsgCallback(const std_msgs::Bool::ConstPtr& msg);

    mavros_msgs::State current_state_;
    int drone_state_;
    int mode_;

};