#include <state_machine/state_machine.hpp>


StateMachine::StateMachine(ros::NodeHandle & nh):
 nh_(nh)
{
    droneMsg_sub = nh_.subscribe<state_machine::DroneMsg>
            ("command", 10, &StateMachine::droneMsgCallback, this);

    roverMsg_sub = nh_.subscribe<std_msgs::Bool>
            ("/rhino/state_machine", 10, &StateMachine::roverMsgCallback, this);

    state_machine_pub = nh_.advertise<state_machine::DroneMsg>
            ("state_machine", 10);
}

void StateMachine::droneMsgCallback(const state_machine::DroneMsg::ConstPtr& msg)
{
    switch (msg->drone_state.data)
    {
        case DRONE_STATES::mission_temp:
            if (msg->mission_status.data == MISSION_STATUS::done)
            {
                mode_ = DRONE_STATES::idle;
                stateMachine();
            }
            break;

        case DRONE_STATES::mission_takeoff:
            if (msg->mission_status.data == MISSION_STATUS::done)
            {
                mode_ = DRONE_STATES::mission_temp;
                stateMachine();
            }
            break;

        case DRONE_STATES::mission_map:
            if (msg->mission_status.data == MISSION_STATUS::done)
            {
                mode_ = DRONE_STATES::mission_land;
                stateMachine();
            }
            break;

        case DRONE_STATES::mission_land:
            if (msg->mission_status.data == MISSION_STATUS::done)
            {
                mode_ = DRONE_STATES::idle;
                stateMachine();
            }
            break;
    }
}

void StateMachine::roverMsgCallback(const std_msgs::Bool::ConstPtr& msg)
{
    if(msg->data)
    {
        ROS_INFO("State Machine: Rover Ready.");
        mode_ = DRONE_STATES::mission_takeoff;
        stateMachine();
    }
}

void StateMachine::stateMachine()
{
    state_machine::DroneMsg drone_msg;
    switch (mode_)
    {
        case DRONE_STATES::initialize:
            ROS_INFO("State Machine: Initialize");
            drone_msg.drone_state.data = DRONE_STATES::initialize;
            state_machine_pub.publish(drone_msg);
            break;

        case DRONE_STATES::mission_temp:
            ROS_INFO("State Machine: Mission Temp");
            drone_msg.drone_state.data = DRONE_STATES::mission_temp;
            state_machine_pub.publish(drone_msg);
            break;

        case DRONE_STATES::mission_takeoff:
            ROS_INFO("State Machine: Mission Map");
            drone_msg.drone_state.data = DRONE_STATES::mission_takeoff;
            state_machine_pub.publish(drone_msg);
            break;

        case DRONE_STATES::mission_map:
            ROS_INFO("State Machine: Mission Map");
            drone_msg.drone_state.data = DRONE_STATES::mission_map;
            state_machine_pub.publish(drone_msg);
            break;

        case DRONE_STATES::mission_land:
            ROS_INFO("State Machine: Mission Land");
            drone_msg.drone_state.data = DRONE_STATES::mission_land;
            state_machine_pub.publish(drone_msg);
            break;

        case DRONE_STATES::idle:
            ROS_INFO("State Machine: Idle");
            drone_msg.drone_state.data = DRONE_STATES::idle;
            state_machine_pub.publish(drone_msg);
            break;
    }
}

int main(int argc, char **argv)
{

    // initialize ROS and the node
    ros::init(argc, argv, "state_machine");
    ros::NodeHandle nh;

    ros::Rate rate(50.0);
    
    StateMachine state_machine(nh);

    while(ros::ok())
    {
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}
